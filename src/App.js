import './App.css';
import React, { useState, useEffect } from "react";
import { DataTable } from "./components/DataTable";


function App() {
  const [posts, setPosts] = useState([]);
  const [filteredPosts, setFilteredPosts] = useState(null);
  const [searchString, setSearchString] = useState('');

  const POSTS_URL = 'https://jsonplaceholder.typicode.com/posts/';

  useEffect(() => {
    fetch(POSTS_URL).then((responce) =>
      responce.json().then((data) => setPosts(data))
    );
  }, []);

  var handleInputChange = (event) => {
    const searchStr = event.target.value;

    if (event.keyCode === 13 || !searchStr) {
      setSearchString(searchStr)
      setFilteredPosts(posts.filter(post => post.body.includes(searchStr)));
    }
  }

  return (
    <div className="App">
      <label>
        Search:
        <input type="text" onKeyDown={handleInputChange} />
      </label>
      <DataTable posts={filteredPosts && searchString ? filteredPosts : posts}
                 searchString={searchString}/>
    </div>
  );
}

export default App;
