import React from "react";

export function DataTable(props) {

  const { posts, searchString } = props;

  let getHighlightedText = (text, highlight) => {
      // Split text on highlight term, include term itself into parts, ignore case
      const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
      let key = 0;
      return <span>{parts.map(part => part.toLowerCase() === highlight.toLowerCase() ? <mark key={key++}>{part}</mark> : part)}</span>;
  }

  return (
    <table>
      <tbody>
        {
        posts.map((post) =>
          (
            <tr key={post.id}>
              <td>{post.id}</td>
              <td>{getHighlightedText(post.body, searchString)}</td>
            </tr>
          )
        )
        }
      </tbody>
    </table>
  );
}
